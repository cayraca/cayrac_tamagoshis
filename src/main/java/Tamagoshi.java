import java.util.Random;

public abstract class Tamagoshi {
    protected String name;
    protected int age = 0;
    protected int energy;
    protected int maxEnergy;
    protected int energyThreshold;
    protected int fun;
    protected int maxFun;
    protected int funThreshold;

    protected static final int MAX_AGE = 10;
    protected static final Random random = new Random();

    public Tamagoshi(String name) {
        this.name = name;
        this.energy = random.nextInt(3) + 3;
        this.maxEnergy = random.nextInt(5) + 5;
        this.energyThreshold = random.nextInt(3) + 3;
        this.fun = random.nextInt(3) + 3;
        this.maxFun = random.nextInt(5) + 5;
        this.funThreshold = random.nextInt(3) + 3;
    }

    public boolean isAlive() {
        return age < MAX_AGE && (energy > 0 || fun > 0);
    }

    public void ageOneTurn() {
        age++;
        decrementStates();
    }

    public String getState() {
        if (energy <= energyThreshold && fun <= funThreshold) {
            return name + ": je suis affamé et je m'ennuie à mourir !";
        } else if (energy <= energyThreshold) {
            return name + ": je suis affamé !";
        } else if (fun <= funThreshold) {
            return name + ": je m'ennuie à mourir !";
        } else {
            return name + ": Tout va bien !";
        }
    }

    public void feed() {
        energy = Math.min(maxEnergy, energy + random.nextInt(3) + 1);
    }

    public void play() {
        fun = Math.min(maxFun, fun + random.nextInt(3) + 1);
    }

    protected abstract void decrementStates();

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
