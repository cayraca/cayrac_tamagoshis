import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {
    private final List<Tamagoshi> tamagoshis = new ArrayList<>();
    private final Scanner scanner = new Scanner(System.in);

    public void start() {
        System.out.println("Entrez le nombre de tamagoshis désiré !");
        int count = Integer.parseInt(scanner.nextLine());

        for (int i = 1; i <= count; i++) {
            System.out.println("Entrez le nom du tamagoshi numéro " + i + " :");
            String name = scanner.nextLine();
            tamagoshis.add(createRandomTamagoshi(name));
        }

        play();
    }

    private void play() {
        int turn = 0;
        while (tamagoshis.stream().anyMatch(Tamagoshi::isAlive)) {
            turn++;
            System.out.println("\n------------ Tour n°" + turn + " -------------");
            tamagoshis.forEach(t -> System.out.println(t.getState()));

            System.out.println("\nNourrir quel tamagoshi ?");
            interact("feed");

            System.out.println("\nJouer avec quel tamagoshi ?");
            interact("play");

            tamagoshis.forEach(Tamagoshi::ageOneTurn);
        }

        endGame();
    }

    private void interact(String action) {
        for (int i = 0; i < tamagoshis.size(); i++) {
            System.out.println("(" + i + ") " + tamagoshis.get(i).getName());
        }
        int choice = Integer.parseInt(scanner.nextLine());
        Tamagoshi selected = tamagoshis.get(choice);
        if ("feed".equals(action)) {
            selected.feed();
            System.out.println(selected.getName() + ": Merci !");
        } else if ("play".equals(action)) {
            selected.play();
            System.out.println(selected.getName() + ": On se marre !");
        }
    }

    private void endGame() {
        System.out.println("\n--------- fin de partie !! ----------------");
        int totalMaxAge = tamagoshis.size() * Tamagoshi.MAX_AGE;
        int totalAge = tamagoshis.stream().mapToInt(Tamagoshi::getAge).sum();
        double score = (double) totalAge / totalMaxAge * 100;

        tamagoshis.forEach(t -> {
            String status = t.isAlive() ? "a survécu et vous remercie :)" : "n'est pas arrivé au bout et ne vous félicite pas :(";
            System.out.println(t.getName() + " qui était un " + t.getClass().getSimpleName() + " " + status);
        });

        System.out.println("Niveau de difficulté : " + tamagoshis.size() + ", score obtenu : " + score + "%");
    }

    private Tamagoshi createRandomTamagoshi(String name) {
        return Tamagoshi.random.nextBoolean() ? new BigEater(name) : new BigPlayer(name);
    }

    public static void main(String[] args) {
        new Game().start();
    }
}
