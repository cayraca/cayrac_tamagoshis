public class BigPlayer extends Tamagoshi {
    public BigPlayer(String name) {
        super(name);
    }

    @Override
    protected void decrementStates() {
        fun -= 2; // Perte de fun plus rapide
        energy--;
    }
}