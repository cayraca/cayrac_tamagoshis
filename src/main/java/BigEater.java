public class BigEater extends Tamagoshi {
    public BigEater(String name) {
        super(name);
    }

    @Override
    protected void decrementStates() {
        energy -= 2; // Consomme plus d'énergie
        fun--;
    }
}
