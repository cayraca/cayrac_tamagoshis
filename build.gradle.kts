plugins {
    id("java")
    id ("org.sonarqube") version "4.2.1.3168"
}


group = "org.example"
version = "1.0-SNAPSHOT"


repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.10.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

sonar {
    properties {
        property("sonar.projectKey", "cayraca_cayrac_tamagoshis_AZO68mE2p6w51ilm7U6-")
        property("sonar.projectName", "Cayrac_Tamagoshis")
        property("sonar.qualitygate.wait", true)
    }
}

tasks.test {
    useJUnitPlatform()
}